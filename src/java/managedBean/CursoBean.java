/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entidades.Curso;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import servicios.CursoFacadeLocal;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.model.UploadedFile;

@Named(value = "cursoBean")
@RequestScoped
public class CursoBean {

    @EJB
    private CursoFacadeLocal cursoFacade;

    private UploadedFile file;
    private String msj;
    private Curso curso;

    public CursoBean() {
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    @PostConstruct
    public void init() {
        this.curso = new Curso();
    }

    public void insert() {

        try {
            //Logica para subir imagen
            InputStream input = file.getInputstream();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];

            for (int length = 0; (length = input.read(buffer)) > 0;) {
                output.write(buffer, 0, length);

            }
            curso.setFoto(output.toByteArray());//Seteamos la imagen en la entidad curso
            cursoFacade.create(curso); //Se envia como parametro la entidad curso en el metodo del EJB
            this.curso = new Curso();
            this.msj = "Curso Insertado Correctamente";
        } catch (Exception e) {
            e.printStackTrace();
            this.msj = "Error" + e.getMessage();
        }
        FacesMessage mensaje = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    //Logica de Listar los registros de la base de datos
    public List<Curso> getLtsCurso() {
        return cursoFacade.findAll();
    }

    //Logica de conexion (ARREGLAR CODIGO)
    Connection con;

    public Connection getConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/proyectoappweb", "root", "");
        } catch (Exception e) {
        }
        return con;
    }

    //Logica listar imagen
    public void listarImg(int id, HttpServletResponse response) {
        Connection con;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from curso where idcurso=" + id;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;

        response.setContentType(
                "image/*");
        try {
            outputStream = response.getOutputStream();
            con = getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                inputStream = rs.getBinaryStream(3);
            }
            bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            int i = 0;
            while ((i = bufferedInputStream.read()) != -1) {
                bufferedOutputStream.write(i);
            }
        } catch (Exception e) {
        }
    }

    //Logica Delete
    public void eliminar(Curso eliminar) {
        try {
            this.cursoFacade.remove(eliminar);
            this.msj = "Curso Eliminado Correctamente";

        } catch (Exception ex) {
            ex.printStackTrace();
            this.msj = "Error" + ex.getMessage();
        }
        FacesMessage mensaje = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

    public void actualizar() {
        try {
            this.cursoFacade.edit(curso);
            this.curso = new Curso();
            this.msj = "Actualizado Con exito";
        } catch (Exception e) {
            e.printStackTrace();
            this.msj = "Error : " + e.getMessage();
        }
        FacesMessage mens = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, mens);
    }

    public void cargarID(Curso curso) {
        this.curso = curso;

    }

    public void limpiar() {
        this.curso = new Curso();
    }
}
